import './App.css';
import React from 'react';
import axios from 'axios';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {login: '', password: ''};
  }

  sendLoginRequest()
  {
    axios.get('/users', {
      params: {
        ID: 1
      }
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  render() {
  return (
    <div className="App">
        <label>Email</label>
        <input type='text' value={this.state.login}></input>
        <input type='text' value={this.state.password}></input>
        <button onClick={this.sendLoginRequest}></button>
    </div>
  );
}
}

export default App;
